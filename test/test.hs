{-# LANGUAGE OverloadedStrings #-}


import           Control.Monad.IO.Class       (MonadIO, liftIO)
import           Control.Monad.Trans.Resource (runResourceT)
import qualified Data.ByteString.Lazy.Char8   as BL
import           Data.Conduit
import qualified Data.Conduit.List            as CL
import           Network.AMQP
import           Network.AMQP.Conduit
import           System.AMQP.Monad


main :: IO ()
main = do
    let queue = newQueue {queueName = "myQueue"}
        exchange = newExchange {exchangeName = "myExchange", exchangeType = "direct"}
        conf= AmqpConf "amqp://guest:guest@192.168.59.103:5672/" queue exchange "myKey"
    runResourceT $ runAMQP conf $ \conn -> do
        liftIO $ putStrLn "test is runnning..."
        liftIO $ mySrc $$ CL.mapM_ putStrLn
        -- sendMsg $$ amqpSendSink conn "key"
        --amqpReceiveSource conn $$ printMsg
        liftIO $ putStrLn "test has been completed"

sendMsg :: (Monad m, MonadIO m) => Source m Message
sendMsg = do
    liftIO $ putStrLn "enter a message: "
    m <- liftIO $ getLine
    yield (newMsg {msgBody = (BL.pack m),msgDeliveryMode = Just Persistent} ) >> sendMsg

printDummy :: (Monad m, MonadIO m) => Sink Message m ()
printDummy = do
    liftIO $ putStrLn "called print dummy: "
    m <- await
    case m of
        Nothing -> printDummy
        Just msg -> liftIO $ putStrLn $ "received message: " ++ (BL.unpack $ msgBody msg)

printMsg :: Sink (Message, Envelope) IO ()
printMsg = do
    m <- await
    case m of
       Nothing -> printMsg
       Just (msg,env) -> do
           liftIO $ ackEnv env
           liftIO $ putStrLn $ "received message: " ++ (BL.unpack $ msgBody msg)



mySrc :: (Monad m, MonadIO m) => Source m String
mySrc = do
    x <- liftIO $ getLine
    if x == "END"
        then return ()
        else yield x >> mySrc



