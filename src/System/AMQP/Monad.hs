{-# LANGUAGE CPP                        #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE StandaloneDeriving         #-}


module System.AMQP.Monad
    (
      AmqpConf (..)
    , AmqpConn (..)
    , ExchangeKey
    , AmqpURI
    , runAMQP

    , connect
    , send
    , receive

    ) where

import           Control.Applicative
import           Control.Exception.Lifted     (bracket)
import           Control.Monad.Base           (MonadBase)
import           Control.Monad.IO.Class       (MonadIO, liftIO)
import           Control.Monad.Reader         (ReaderT, runReaderT)
import           Control.Monad.Reader.Class   (MonadReader)
import           Control.Monad.Trans          (MonadTrans)
import           Control.Monad.Trans.Control  (MonadBaseControl)
import           Control.Monad.Trans.Resource (MonadResource, MonadThrow)
import           Data.Text                    (Text)
import           Network.AMQP

data AmqpConn = AmqpConn
    { amqpConn :: Connection
    , amqpChan :: Channel
    , amqpConf :: AmqpConf
   }

data AmqpConf = AmqpConf
    { amqpUri       :: String
    , amqpQueue     :: QueueOpts
    , amqpExchange  :: ExchangeOpts
    , amqpExchanKey :: ExchangeKey
    }

newtype AMQP m a = AMQP { unAMQP :: ReaderT AmqpConf m a }
    deriving (Functor, Applicative, Monad, MonadIO, MonadReader AmqpConf, MonadThrow, MonadTrans)

deriving instance MonadBase IO m => MonadBase IO (AMQP m)
deriving instance MonadResource m => MonadResource (AMQP m)

type ExchangeKey = Text
type AmqpURI = String

-- internal

connect :: (MonadIO m) => AmqpConf -> m AmqpConn
connect conf = do
    -- make a connection and a channel of the connection.
    conn <-liftIO $  openConnection'' uri
    chan <- liftIO $ openChannel conn

    -- debug
    liftIO $ putStrLn ("URI = " ++ (show (amqpUri conf)))
    liftIO $ putStrLn ("exchange name = " ++ (show eName))
    liftIO $ putStrLn ("ecchange key = " ++ (show (key)))
    liftIO $ putStrLn ("queue name= = " ++ (show (qName)))

    -- declare a queue, exchange and binding
    liftIO $ declareExchange chan (amqpExchange conf)
    _ <- liftIO $ declareQueue chan (amqpQueue conf)
    liftIO $ bindQueue chan qName eName key


    return $ AmqpConn conn chan conf
    where
        qName = queueName (amqpQueue conf)
        key = amqpExchanKey conf
        uri = fromURI (amqpUri conf)
        eName = exchangeName (amqpExchange conf)

disconnect :: (MonadIO m) => AmqpConn -> m ()
disconnect conn = do
    liftIO $ closeChannel (amqpChan conn)
    liftIO $ closeConnection (amqpConn conn)

-- receive :: (MonadIO m) => AmqpConn -> AmqpEnv -> m (Either (String (Message, Envelope)))
receive :: (MonadIO m) => AmqpConn -> m (Message, Envelope)
receive conn = do
    msg <- liftIO $ getMsg (amqpChan conn) Ack (queueName (amqpQueue (amqpConf conn)))
    case msg of
        Nothing -> receive conn
        Just (m,e) -> return (m,e)

send :: (MonadIO m) =>  AmqpConn ->  ExchangeKey -> Message -> m ()
send conn key msg = do
    liftIO $ putStrLn ("key = " ++ show key ++ ", msg = " ++ show msg)
    liftIO $ publishMsg
        (amqpChan conn)
        (exchangeName (amqpExchange (amqpConf conn))) key msg
    liftIO $ putStrLn $ "key = " ++ show key
    liftIO $ putStrLn $ "msg = " ++ show msg

-- runAMQP :: (MonadIO m, MonadBaseControl IO m)
--     => AmqpEnv
--     -> AMQP m a
--     -> m a
runAMQP :: (MonadIO m, MonadBaseControl IO m)
        => AmqpConf
        -> (AmqpConn -> m a)
        -> m a
runAMQP conf f =


    bracket
        (do
            liftIO $ putStrLn ""

            connect conf)
        disconnect
        f
        -- (\ctx -> runReaderT (unAMQP (f ctx)) conf )

    where
        qName = queueName (amqpQueue conf)
        key = amqpExchanKey conf
        uri = fromURI (amqpUri conf)
        eName = exchangeName (amqpExchange conf)


