{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module Network.AMQP.Conduit (
      amqpReceiveSource
    , amqpSendSink
    ) where


import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Data.Conduit           (Sink, Source, await, yield)
import           Network.AMQP
import           System.AMQP.Monad

amqpReceiveSource :: MonadIO m => AmqpConn -> Source m (Message, Envelope)
amqpReceiveSource conn = loop
    where
        loop = (receive conn) >>= yield >> loop

amqpSendSink :: MonadIO m => AmqpConn -> ExchangeKey -> Sink Message m ()
amqpSendSink conn key = loop
    where
        loop = await >>= maybe ((liftIO (putStrLn "Nothing")) >> return ()) (\v -> (send conn key v) >> loop)


